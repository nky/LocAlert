# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/NKY/Documents/Develop/Android/SDK/android-sdk-macosx/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
#-libraryjars libs/TencentLocationSDK_v4.5.8.jar
#-libraryjars libs/TencentMapSDK_Raster_v1.2.3.20160630.jar
#-libraryjars <java.home>/lib/rt.jar

#-dontskipnonpubliclibraryclassmembers
-optimizationpasses 5
-dontpreverify #不预校验
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/* #优化

-dontwarn javax.persistence.*
-dontwarn sun.misc.Unsafe
-dontwarn org.slf4j.*
-dontwarn javax.lang.model.element.*
-dontwarn javax.**
-dontwarn  org.eclipse.**

#V4&V7
-keep class android.support.v4.**{*;}
-keep class android.support.v7.**{*;}
-keep class android.support.design.**{*;}

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service

# will keep line numbers and file name obfuscation
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

# Data
-keep class com.acnt.location.alert.data.**

# RX*
-keep class rx.**{*;}
#-keep class sun.misc.**
#-keep class

# Logger
-keep class com.alterego.advancedandroidlogger.**{*;}
-keep class org.slf4j.**

# ButterKnife
-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }
-keep class **$$ViewInjector { *; }
-dontwarn butterknife.internal.**

# EventBus3
-keepattributes *Annotations*
-keepclassmembers class **{
     @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }


# Ormlite
# OrmLite uses reflection
-keep class com.j256.**
-keepclassmembers class com.j256.** { *; }
-keep enum com.j256.**
-keepclassmembers enum com.j256.** { *; }
-keep interface com.j256.**
-keepclassmembers interface com.j256.** { *; }

-keepclassmembers class * {
  public <init>(android.content.Context);
}


-keep class  org.apache.harmony.**
-keep class org.joda.**
-keep class org.eclipse.jdt.annotation.*
-keep class javax.persistence.*
-keep class javax.lang.model.**
#Tencent search sdk
#-libraryjars ../libs/TencentSearch1.1.2.16095.jar
-keep class com.tencent.lbssearch.**{*;}
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }


#Tencent map sdk
#-libraryjars libs/TencentMapSDK_Raster_v1.2.3.20160630.jar
-keep class com.tencent.mapsdk.**{*;}
-keep class com.tencent.tencentmap.**{*;}

#Tencent locate sdk
#-libraryjars libs/TencentLocationSDK_v4.4.6_r206631_151119_1441.jar
-keep class com.tencent.map.**{*;}
-keep class com.tencent.tencentmap.**{*;}
-keep class ct.**{*;}
-dontwarn ct.**

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn org.eclipse.jdt.annotation.**
-dontwarn android.location.Location
-dontwarn com.tencent.**
-dontnote ct.**

#-keep class com.acnt.location.alert.**{*;}
#-keepclasseswithmembers class com.acnt.location.alert.**

# Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keepresourcexmlelements manifest/application/meta-data@value=GlideModule
