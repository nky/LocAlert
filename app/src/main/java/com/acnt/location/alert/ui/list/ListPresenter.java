package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.data.entities.AlertLocation;

import android.content.Intent;

import java.util.List;

/**
 * List的Presenter
 * Created by NKY on 7/30/16.
 */
public interface ListPresenter {


    void onItemClicked(AlertLocation location);

    void deleteLocation(AlertLocation location);

    void loadData();

    void onBackPressed();
}
