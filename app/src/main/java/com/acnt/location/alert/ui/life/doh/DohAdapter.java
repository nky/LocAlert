package com.acnt.location.alert.ui.life.doh;

import com.acnt.location.alert.R;
import com.acnt.location.alert.ui.life.entities.DayOnHistory;
import com.bumptech.glide.Glide;

import org.apache.commons.lang3.StringUtils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 定义列表的适配器
 * Created by NKY on 9/20/16.
 */

class DohAdapter extends RecyclerView.Adapter<DohAdapter.DohViewHolder> {


    private final List<DayOnHistory> mItems = new ArrayList<>();
    private final Context mContext;
    private final LayoutInflater mInflater;
    private OnItemActionClickedListener mListener;

    public DohAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public void setOnItemActionClickedListener(OnItemActionClickedListener listener) {
        mListener = listener;
    }

    public void updateData(List<DayOnHistory> histories) {
        mItems.clear();
        mItems.addAll(histories);
        notifyDataSetChanged();
    }

    @Override
    public DohViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mInflater.inflate(R.layout.item_doh, parent, false);
        return new DohViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DohViewHolder holder, int position) {
        final DayOnHistory dayOnHistory = mItems.get(position);
        holder.bindData(dayOnHistory);
        if (mListener!=null) {
            holder.mBtnAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onItemClicked(dayOnHistory);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    interface OnItemActionClickedListener{
        void onItemClicked(DayOnHistory history);
    }


    static class  DohViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tv_doh_title)
        TextView mTvTitle;
        @BindView(R.id.tv_doh_lunar)
        TextView mTvLunar;
        @BindView(R.id.tv_doh_description)
        TextView mTvDescription;
        @BindView(R.id.iv_doh_pic)
        ImageView mIvPic;
        @BindView(R.id.btn_doh_action)
        ImageButton mBtnAction;

        public DohViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(DayOnHistory history) {
            mTvDescription.setText(history.getDes());
            mTvTitle.setText(history.getTitle());
            mTvLunar.setText(history.getLunar());
            String pic = history.getPic();
            if (StringUtils.isEmpty(pic)) {
                mIvPic.setVisibility(View.GONE);
            } else {
                mIvPic.setVisibility(View.VISIBLE);
                Glide.with(itemView.getContext())
                        .load(pic)

                        .centerCrop()
                        .crossFade()
                        .into(mIvPic);
            }
        }
    }
}
