package com.acnt.location.alert.data.entities;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 定义网络定位的位置封装
 * Created by NKY on 8/7/16.
 */
@Data
@Accessors(prefix = "m")
@ToString
public class Location {
    private double mLatitude;
    private double mLongitude;
    private float mAccuracy;

}
