package com.acnt.location.alert.inject;

import com.acnt.location.alert.ui.add.AddAlertLocationActivity;
import com.acnt.location.alert.ui.main.MapsActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Component
 * Created by NKY on 7/28/16.
 */
@Singleton
@Component(modules = DataModule.class)
public interface DataComponent {
    void injectApp(AddAlertLocationActivity addAlertLocationActivity);
}
