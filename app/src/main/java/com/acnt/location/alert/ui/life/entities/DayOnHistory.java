package com.acnt.location.alert.ui.life.entities;

import lombok.Data;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * 历史上的今天
 * Created by hzniukuiyuan on 2016/9/19.
 */
@Data
public class DayOnHistory implements Comparable<DayOnHistory>{

    @SerializedName("day")
    @Expose
    private int day;
    @SerializedName("des")
    @Expose
    private String des;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("lunar")
    @Expose
    private String lunar;
    @SerializedName("month")
    @Expose
    private int month;
    @SerializedName("pic")
    @Expose
    private String pic;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("year")
    @Expose
    private int year;

    @Override
    public int compareTo(DayOnHistory dayOnHistory) {
        return Integer.compare(year, dayOnHistory.year);
    }

}