package com.acnt.location.alert.ui.bg;

import com.acnt.location.alert.R;
import com.acnt.location.alert.app.App;
import com.acnt.location.alert.config.Constants;
import com.acnt.location.alert.data.AlertLocationRepository;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.data.entities.Location;
import com.acnt.location.alert.inject.DataModule;
import com.acnt.location.alert.ui.main.MapsActivity;
import com.acnt.location.alert.util.EventBusUtil;
import com.acnt.location.alert.util.LocationUtil;
import com.acnt.location.alert.util.ObjectUtil;

import org.greenrobot.eventbus.Subscribe;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class LocationService extends Service {
    LocationComponent mComponent;
    @Inject
    LocationUtil mLocationUtil;
    private NotificationCompat.Builder mBuilder;
    private Subscription mSubscribe;
    @Inject
    AlertLocationRepository mRepository;

    public LocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (mComponent == null) {
            mComponent = DaggerLocationComponent
                    .builder()
                    .dataModule(new DataModule(this))
                    .locationModule(new LocationModule(this))
                    .build();
            mComponent.injectApp(this);
        }

        EventBusUtil.register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        App.L.d("onStartCommand: " + mComponent + "\n" + mLocationUtil);
        String action = intent.getAction();

        if (action.equals(Constants.ACTION_START_SERVICE)) {
            startService();
        } else if (action.equals(Constants.ACTION_STOP_SERVICE)) {
            tryStopService();
        } else if (action.equals(Constants.ACTION_STOP_ALARM)) {
            stopAlarm();
        }
        return START_STICKY;

    }

    private void tryStopService() {
        stopForeground(true);
        stopSelf();
        stopAlarm();
        mLocationUtil.recycle();
    }



    private void startService() {


        Intent notificationIntent = new Intent(this, MapsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.navigation);
        Intent close = new Intent(this, LocationService.class);
        close.setAction(Constants.ACTION_STOP_ALARM);
        PendingIntent closeIntent = PendingIntent.getService(this, 0, close, 0);

        mBuilder = new NotificationCompat.Builder(this);
        Notification notification = mBuilder
                .setContentTitle("可以安心的睡觉了")
                .setTicker("What's this?")
                .setContentText("少年, 放心的休息吧, 到地方我来提醒你!")
                .setSmallIcon(R.mipmap.ic_launcher)
                .addAction(R.drawable.navigation, "Close", closeIntent)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true)

                .build();
        startForeground(101, notification);

        mSubscribe = mLocationUtil
                .requestCurrLocation()
                .subscribe(new Subscriber<Location>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        App.L.e("LocationService#onError", e);
                        EventBusUtil.post(e);
                    }

                    @Override
                    public void onNext(Location location) {
                        App.L.info("LocationService#onNext", location.toString());
                        EventBusUtil.post(location);
                    }
                });
    }

    private boolean mIsAlarming = false;
    private MediaPlayer mMediaPlayer;
    private Vibrator mVibrator;
    long[] pattern = {400, 400, 500};
    private void alarm() {
        if (mIsAlarming) {
            return;
        }

        if (mMediaPlayer == null) {
            mMediaPlayer = MediaPlayer.create(this, getDefaultRingtoneUri(this));
        }
        mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        mVibrator.vibrate(pattern, 0);

        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
        mIsAlarming = true;
    }

    @Override
    public void onDestroy() {
        EventBusUtil.unregister(this);
        if (mSubscribe != null && !mSubscribe.isUnsubscribed()) {
            mSubscribe.unsubscribe();
        }
        super.onDestroy();
    }

    @Subscribe
    public void onLocationChanged(Location location) {
        checkAlert(location);
    }


    private void checkAlert(final Location location) {
        mRepository
                .queryAll()
                .filter(new Func1<List<AlertLocation>, Boolean>() {
                    @Override
                    public Boolean call(List<AlertLocation> alertLocations) {
                        return !ObjectUtil.isEmpty(alertLocations);
                    }
                })
                .flatMap(new Func1<List<AlertLocation>, Observable<AlertLocation>>() {
                    @Override
                    public Observable<AlertLocation> call(List<AlertLocation> locations) {
                        return Observable.from(locations);
                    }
                })
                .filter(new Func1<AlertLocation, Boolean>() {
                    @Override
                    public Boolean call(AlertLocation aLocation) {
                        return aLocation.contains(location);
                    }
                })
                .first()
                .subscribe(new Observer<AlertLocation>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        App.L.e("Ooops", e);
                    }

                    @Override
                    public void onNext(AlertLocation alertLocation) {
                        App.L.debug("Find One!: " + alertLocation.getAlertLabel());
                        EventBusUtil.post(Boolean.TRUE);

                    }
                });

    }

    @Subscribe
    public void alarming(Boolean alarm) {
        alarm();
    }

    private void stopAlarm() {
        if (!mIsAlarming) {
            return;
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
       mVibrator.cancel();
        Observable.just(1)
                .delay(60, java.util.concurrent.TimeUnit.SECONDS)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        mIsAlarming = false;
                    }
                });


    }

    /**
     * 获取的是铃声的Uri
     */
    public static Uri getDefaultRingtoneUri(Context ctx) {
        return RingtoneManager.getActualDefaultRingtoneUri(ctx, RingtoneManager.TYPE_ALARM);

    }
}
