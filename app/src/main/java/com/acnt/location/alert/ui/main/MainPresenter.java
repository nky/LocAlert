package com.acnt.location.alert.ui.main;

import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.data.entities.Location;
import com.tencent.mapsdk.raster.model.LatLng;

/**
 * 定义presenter
 * Created by NKY on 7/31/16.
 */
public interface MainPresenter {



    /**
     * 处理添加成功的处理
     * @param location 已经添加成功的数据
     */
    void handleAddSuccess(AlertLocation location);

    /**
     * 长按 地图的处理
     * @param latLng
     */
    void onPositionPressed(Location latLng);

    /**
     * 获取所有的
     */
    void loadAllLocations();


    void onShowListPressed();


    void onDestroy();


    void onRequestLocationPressed();

}
