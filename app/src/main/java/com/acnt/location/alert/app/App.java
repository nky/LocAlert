package com.acnt.location.alert.app;

import com.acnt.location.alert.util.ToastUtil;
import com.alterego.advancedandroidlogger.implementations.AndroidLogger;
import com.alterego.advancedandroidlogger.implementations.DetailedAndroidLogger;
import com.alterego.advancedandroidlogger.interfaces.IAndroidLogger;

import android.support.multidex.MultiDexApplication;

/**
 * 应用的入口
 * Created by NKY on 7/27/16.
 */
public class App extends MultiDexApplication {
    public  static IAndroidLogger L;

    @Override
    public void onCreate() {
        super.onCreate();
        L = new AndroidLogger("LocAlert", IAndroidLogger.LoggingLevel.VERBOSE);
        ToastUtil.init(this);
    }
}
