package com.acnt.location.alert.ui.life.doh;

import dagger.Module;
import dagger.Provides;

/**
 * 定义Doh的module类
 * Created by NKY on 9/20/16.
 */
@Module(includes = AbsDohModuleProvider.class)
class DohModule {

    private final DohView mView;

    public DohModule(DohView view) {
        mView = view;
    }

    @Provides
    public DohView provideView() {
        return mView;
    }
}
