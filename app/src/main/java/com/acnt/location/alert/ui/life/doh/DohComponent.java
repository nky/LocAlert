package com.acnt.location.alert.ui.life.doh;

import com.acnt.location.alert.ui.life.ActivityScope;
import com.acnt.location.alert.ui.life.RepositoryComponent;

import dagger.Component;

/**
 * 定义组件
 * Created by NKY on 9/20/16.
 */

@ActivityScope
@Component(dependencies = RepositoryComponent.class, modules = DohModule.class)
interface DohComponent {

    void inject(DohActivity activity);
}
