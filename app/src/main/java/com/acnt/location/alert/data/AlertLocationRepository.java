package com.acnt.location.alert.data;

import com.acnt.location.alert.data.entities.AlertLocation;

import java.util.List;

import rx.Observable;

/**
 * 定义报警位置的数据库
 * Created by NKY on 7/28/16.
 */
public interface AlertLocationRepository {

    Observable<AlertLocation> save(AlertLocation location);

    Observable<AlertLocation> remove(AlertLocation location);

    Observable<List<AlertLocation>> queryAll();

}
