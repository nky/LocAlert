package com.acnt.location.alert.ui.list;

import dagger.Binds;
import dagger.Module;

/**
 * 抽象类的Bind
 * Created by NKY on 7/30/16.
 */
@Module
abstract class LogicModule {
    @Binds
    public abstract ListPresenter providePresenter(ListPresenterImpl presenter);

    @Binds
    public abstract Interactor provideIteractor(InteractorImpl interactor);

}
