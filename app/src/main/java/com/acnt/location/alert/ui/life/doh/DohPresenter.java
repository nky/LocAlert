package com.acnt.location.alert.ui.life.doh;

/**
 * 定义历史上的今天的Presenter
 * Created by NKY on 9/20/16.
 */

interface DohPresenter {

    void loadData();
}
