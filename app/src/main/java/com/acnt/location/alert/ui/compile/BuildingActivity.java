package com.acnt.location.alert.ui.compile;

import com.acnt.location.alert.R;
import com.acnt.location.alert.app.App;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BuildingActivity extends AppCompatActivity implements BuildingView {

    BuildingPresenter mPresenter;
    @BindView(R.id.pb_loading)
    ProgressBar mPbLoading;
    @BindView(R.id.rcv_logs)
    RecyclerView mRcvLogs;
    SimpleAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_building);
        ButterKnife.bind(this);
        mPresenter = new BuildingPresenterImpl(this);
        mPresenter.loadBuildingInfo();

        mAdapter = new SimpleAdapter(this);
        mRcvLogs.setLayoutManager(new LinearLayoutManager(this));
        mRcvLogs.setAdapter(mAdapter);

    }

    @Override
    public void showBuildInfo(List<String> buildings) {
        App.L.info("< Got BuildInfo >ShowInfo: " + buildings.size());
        mAdapter.updateData(buildings);
        mPbLoading.setVisibility(View.GONE);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(String msg) {
        App.L.d("Error: " + msg);
        mPbLoading.setVisibility(View.GONE);
    }

    @Override
    public void showProgressView() {
        App.L.info("OnPress");
        mPbLoading.setVisibility(View.VISIBLE);

    }
}

class SimpleViewHolder extends RecyclerView.ViewHolder{

    TextView mTvData;

    public SimpleViewHolder(View itemView) {
        super(itemView);
        mTvData = (TextView) itemView.findViewById(android.R.id.text1);

    }

    public void bind(String data) {
        mTvData.setText(data);
    }

}
class SimpleAdapter extends RecyclerView.Adapter<SimpleViewHolder>{
    private Context mContext;
    private final LayoutInflater mInflater;
    private final List<String> mData = new LinkedList<>();

    public SimpleAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    public void updateData(List<String> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SimpleViewHolder(mInflater.inflate(android.R.layout.simple_list_item_1, parent,false));
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
