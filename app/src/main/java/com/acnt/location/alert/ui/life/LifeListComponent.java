package com.acnt.location.alert.ui.life;

import dagger.Component;

/**
 * 定义该列表页面的Component组件
 * Created by NiuKuiYuan on 2016/9/20.
 */
@ActivityScope
@Component(dependencies = RepositoryComponent.class, modules = LifeListModule.class)
interface LifeListComponent {
    void inject(LifeActivity activity);
}
