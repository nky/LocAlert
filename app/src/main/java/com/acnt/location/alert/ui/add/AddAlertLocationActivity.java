package com.acnt.location.alert.ui.add;

import com.acnt.location.alert.R;
import com.acnt.location.alert.app.App;
import com.acnt.location.alert.config.Constants;
import com.acnt.location.alert.data.AlertLocationRepository;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.inject.DaggerDataComponent;
import com.acnt.location.alert.inject.DataComponent;
import com.acnt.location.alert.inject.DataModule;
import com.acnt.location.alert.util.ToastUtil;
import com.jakewharton.rxbinding.widget.RxTextView;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;
import rx.functions.Action1;

public class AddAlertLocationActivity extends AppCompatActivity {
    DataComponent mDataComponent;
    @Inject
    AlertLocationRepository mRepository;
    @BindView(R.id.tilyt)
    TextInputLayout mTilytLabel;
    @BindView(R.id.tilyt_radius)
    TextInputLayout mTilytRadius;

    @BindView(R.id.edt_label)
    TextInputEditText mEdtLabel;
    @BindView(R.id.edt_radius)
    TextInputEditText mEdtRadius;

    @BindView(R.id.btn_cancel)
    Button mBtnCancel;
    @BindView(R.id.btn_save)
    Button mBtnSave;

    AlertLocation mLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_add_alert_location);
        ButterKnife.bind(this);
        setupComponents();
        parseIntent();
        setupValidator();
    }


    private void parseIntent() {
        mLocation = getIntent().getParcelableExtra(Constants.KEY_LOCATION);
        if (mLocation == null) {
            ToastUtil.showMessage("位置不能为空!!");
            finish();
        }
    }

    private void setupValidator() {
        RxTextView.textChanges(mEdtLabel).subscribe(new Action1<CharSequence>() {
            @Override
            public void call(CharSequence charSequence) {
                if (TextUtils.isEmpty(charSequence)) {
                    mTilytLabel.setError(getString(R.string.err_label_empty));
                } else {
                    mTilytLabel.setErrorEnabled(false);
                }
            }
        });

        RxTextView.textChanges(mEdtRadius).subscribe(new Action1<CharSequence>() {
            @Override
            public void call(CharSequence charSequence) {
                if (TextUtils.isEmpty(charSequence) || !TextUtils.isDigitsOnly(charSequence)) {
                    mTilytRadius.setError(getString(R.string.err_radius_empty));
                } else {
                    mTilytRadius.setErrorEnabled(false);
                }
            }
        });
    }

    private void setupComponents() {
        mDataComponent = DaggerDataComponent
                .builder()
                .dataModule(new DataModule(this))
                .build();
        mDataComponent.injectApp(this);
    }

    @OnClick(R.id.btn_save)
    public void onSave() {
        if (TextUtils.isEmpty(mEdtLabel.getText().toString())) {
            mTilytLabel.setError(getString(R.string.err_label_empty));
            return;
        }

        mTilytLabel.setErrorEnabled(false);

        if (TextUtils.isEmpty(mEdtRadius.getText().toString())) {
            mTilytRadius.setError(getString(R.string.err_radius_empty));
            return;
        }

        mTilytRadius.setErrorEnabled(false);

        mLocation.setCreateDate(System.currentTimeMillis());
        mLocation.setAlertLabel(mEdtLabel.getText().toString());
        mLocation.setAlertRadius(Long.parseLong(mEdtRadius.getText().toString()));

        disableInput();
        mRepository
                .save(mLocation)
                .subscribe(new Subscriber<AlertLocation>() {
                    @Override
                    public void onCompleted() {
                        App.L.d("Save Completed....");
                        enableInput();
                    }

                    @Override
                    public void onError(Throwable e) {
                        enableInput();
                        ToastUtil.showMessage("Save Error: " + e);
                        App.L.e("Save Data: ", e);
                    }

                    @Override
                    public void onNext(AlertLocation location) {
                        //业务逻辑, 成功
                        App.L.d("Save success: " + location);
                        Intent intent = new Intent();
                        intent.putExtra(Constants.KEY_LOCATION, location);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });


    }

    private void disableInput() {
        mEdtLabel.setEnabled(false);
        mEdtRadius.setEnabled(false);
        mBtnCancel.setEnabled(false);
        mBtnSave.setEnabled(false);

    }

    private void enableInput() {
        mEdtLabel.setEnabled(true);
        mEdtRadius.setEnabled(true);
        mBtnCancel.setEnabled(true);
        mBtnSave.setEnabled(true);
    }

    @OnClick(R.id.btn_cancel)
    public void cancel() {
        finish();
    }
}
