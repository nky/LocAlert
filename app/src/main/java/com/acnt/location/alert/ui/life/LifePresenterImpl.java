package com.acnt.location.alert.ui.life;

import com.acnt.location.alert.app.App;
import com.acnt.location.alert.ui.life.entities.DayOnHistory;
import com.acnt.location.alert.ui.life.entities.LifeItem;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * 业务逻辑调用的实现
 * Created by hzniukuiyuan on 2016/9/19.
 */
class LifePresenterImpl implements LifePresenter{

    @Inject
    LifeListView mLifeListView;
    @Inject
    LifeDataRepository mRepository;


    @Inject
    public LifePresenterImpl() {
    }

    @Override
    public void loadAllData() {
        mLifeListView.showProgressView();
        mRepository.loadSupportLifeItems()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<LifeItem>>() {
                    @Override
                    public void onCompleted() {
                        mLifeListView.hideProgressView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mLifeListView.hideProgressView();
                    }

                    @Override
                    public void onNext(List<LifeItem> items) {
                        mLifeListView.showLifeList(items);
                    }
                });

        mRepository.daysOnHistory()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<DayOnHistory>>() {
                    @Override
                    public void onCompleted() {
                        App.L.d("daysOnHistory....: onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        App.L.e("daysOnHistory....: onError" , e);
                    }

                    @Override
                    public void onNext(List<DayOnHistory> dayOnHistories) {
                        App.L.d("daysOnHistory....: onNext: " + dayOnHistories);
                    }
                });

    }

    @Override
    public void onItemClicked(LifeItem item) {
        mLifeListView.showDohView();
    }
}
