package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.R;
import com.acnt.location.alert.data.entities.AlertLocation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 数据的显示适配器
 * Created by NKY on 7/30/16.
 */
public class AlertLocationAdapter extends RecyclerView.Adapter<AlertLocationAdapter.LocationViewHolder> {

    private List<AlertLocation> mLocations = new ArrayList<>();
    private Context mContext;
    private final LayoutInflater mInflater;

    public AlertLocationAdapter( Context context) {
        mLocations = new ArrayList<>();
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    public void updateData(List<AlertLocation> locations) {
        mLocations.clear();
        mLocations.addAll(locations);
        notifyDataSetChanged();
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LocationViewHolder(mInflater.inflate(R.layout.item_location, parent, false));
    }

    @Override
    public void onBindViewHolder(final LocationViewHolder holder, int position) {
        final AlertLocation location = mLocations.get(position);
        holder.bind(location);
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mOnItemPressedListener.onItemClicked(location, holder.getAdapterPosition());
                return false;
            }
        });
    }

    OnItemViewClickListener mOnItemPressedListener;
    public void setItemClickListener(OnItemViewClickListener listener) {
        mOnItemPressedListener = listener;
    }



    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    /**
     * 定义该View的点击事件
     */
    public interface OnItemViewClickListener{
        void onItemClicked(AlertLocation location, int position);
    }


   static class LocationViewHolder extends RecyclerView.ViewHolder{
        //@BindView(R.id.tv_location_label)
        TextView mTvLabel;
        //@BindView(R.id.tv_location_radius)
        TextView mTvRadius;
        //@BindView(R.id.tv_location_date)
        TextView mTvDate;

        public LocationViewHolder(View itemView) {
            super(itemView);
            //ButterKnife.bind(this.itemView);
            mTvLabel = (TextView) itemView.findViewById(R.id.tv_location_label);
            mTvRadius = (TextView) itemView.findViewById(R.id.tv_location_radius);
            mTvDate = (TextView) itemView.findViewById(R.id.tv_location_date);
        }

        public void bind(AlertLocation location){
            mTvLabel.setText(location.getAlertLabel());
            mTvRadius.setText(String.valueOf(location.getAlertRadius()));
            mTvDate.setText(DateFormat.getMediumDateFormat(itemView.getContext()).format(new Date(location.getCreateDate())));

        }

    }
}


