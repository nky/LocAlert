package com.acnt.location.alert.ui.life;

import android.app.Activity;
import android.content.Context;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

/**
 * 网络的组件库.
 * Created by NiuKuiYuan on 2016/9/20.
 */
@Module(includes = AbsRepositoryModuleProvider.class)
public class RepositoryModule {

    protected Activity mContext;


    @Provides
    public Context provideContext() {
        return mContext;
    }

    @Provides
    public Interceptor provideKeyInterceptor(@Named("ApiKey") final String key) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request.url();
                String urlString = url.toString();
                if (request.method().equals("GET") && urlString.contains("juheapi.com")){
                    if (StringUtils.isEmpty(url.queryParameter("key"))){
                        url =   url.newBuilder()
                                .addQueryParameter("key", key)
                                .addQueryParameter("v", "1.0")
                                .build();
                        request = request.newBuilder().url(url).build();
                    }
                }
                return chain.proceed(request);
            }
        };
    }

    @Provides
    public OkHttpClient provideHttpClient(Interceptor keyInterceptor) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return new OkHttpClient.Builder()
                .addInterceptor(keyInterceptor)
                .addInterceptor(loggingInterceptor)
                .build();
    }


    @Provides
    public Retrofit provideRetrofit(OkHttpClient client, @Named("BaseUrl") String url ) {

        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .baseUrl(url)
                .client(client)
                .build();
    }

    @Provides
    @Named("BaseUrl")
    public String provideUrl() {
        return "";
    }

    @Provides
    @Named("ApiKey")
    public String provideApiKey() {
        return "";
    }

    @Provides
    public LifeService provideLifeService(Retrofit retrofit) {
        return retrofit.create(LifeService.class);
    }

}
