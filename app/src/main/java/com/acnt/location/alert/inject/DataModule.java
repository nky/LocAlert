package com.acnt.location.alert.inject;

import com.acnt.location.alert.data.AlertLocationRepository;
import com.acnt.location.alert.data.DbHelper;
import com.acnt.location.alert.data.impl.AlertLocationRepositoryImpl;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module
 * Created by NKY on 7/28/16.
 */
@Module
public class DataModule {

    private final Context mContext;

    public DataModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    public AlertLocationRepository provideAlertLocationRepository(DbHelper dbHelper) {
        return new AlertLocationRepositoryImpl(dbHelper);
    }

    @Provides
    public Context provideContext() {
        return mContext;
    }

}
