package com.acnt.location.alert.ui.life;

import com.acnt.location.alert.ui.life.entities.LifeItem;

import java.util.List;

/**
 * View的定义
 * Created by hzniukuiyuan on 2016/9/19.
 */
interface LifeListView {

    void showLifeList(List<LifeItem> items);

    void showProgressView();

    void hideProgressView();

    void showDohView();
}
