package com.acnt.location.alert.ui.life;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.acnt.location.alert.R;
import com.acnt.location.alert.app.App;
import com.acnt.location.alert.ui.life.doh.DohActivity;
import com.acnt.location.alert.ui.life.entities.LifeItem;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LifeActivity extends AppCompatActivity implements LifeListView {

    @BindView(R.id.rv_life_info)
    RecyclerView mRcvLifeInfo;
    @Inject
    LifePresenter mPresenter;

    private LifeAdapter mAdapter;
    private Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life);
        ButterKnife.bind(this);
        setupComponent();
        setupViews();
        mPresenter.loadAllData();
    }

    private void setupViews() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        mRcvLifeInfo.setHasFixedSize(false);
        mRcvLifeInfo.setLayoutManager(layout);

        mAdapter = new LifeAdapter(this);
        mRcvLifeInfo.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(mPresenter);
    }

    private void setupComponent() {
        RepositoryComponent repositoryComponent = DaggerRepositoryComponent.builder()
                .repositoryModule(new LifeListSpecModule(this))
                .build();

        DaggerLifeListComponent.builder()
                .repositoryComponent(repositoryComponent)
                .lifeListModule(new LifeListModule(this))
                .build()
                .inject(this);

    }

    @Override
    public void showLifeList(List<LifeItem> items) {
        App.L.info("showLifeList called" + items);
        mAdapter.updateAllData(items);
    }

    @Override
    public void showProgressView() {
        App.L.info("showProgressView called");
        mDialog = ProgressDialog.show(this, "正在加载数据", "正在拼命的加载数据中...", true, false);
    }

    @Override
    public void hideProgressView() {
        App.L.info("hideProgressView called");
        if (mDialog != null  && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public void showDohView() {
        Intent intent = new Intent(this, DohActivity.class);
        startActivity(intent);
    }
}
