package com.acnt.location.alert.ui.life;

/**
 * 定义每一个组件定义的一个自己特性的实现类, 提供具体的配置
 * Created by NiuKuiYuan on 2016/9/20.
 */

class LifeListSpecModule extends RepositoryModule {
    private static final String KEY_TOH = "aec6dadb42bcb95ffa5078da43bc7316";


    public LifeListSpecModule(LifeActivity activity) {
        mContext = activity;
    }

    @Override
    public String provideUrl() {
        return "http://api.juheapi.com";
    }

    @Override
    public String provideApiKey() {
        return KEY_TOH;
    }
}
