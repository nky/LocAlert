package com.acnt.location.alert.ui.compile;

import android.content.Context;

import java.util.List;

/**
 * 定义编译信息页面的UI
 * Created by NKY on 8/13/16.
 */
public interface BuildingView {

    void showBuildInfo(List<String> buildings);

    Context getContext();

    void showError(String msg);

    void showProgressView();
}
