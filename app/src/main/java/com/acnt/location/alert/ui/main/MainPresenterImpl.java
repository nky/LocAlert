package com.acnt.location.alert.ui.main;

import com.acnt.location.alert.app.App;
import com.acnt.location.alert.data.AlertLocationRepository;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.data.entities.Location;
import com.acnt.location.alert.util.EventBusUtil;
import com.acnt.location.alert.util.LocationUtil;
import com.acnt.location.alert.util.ObjectUtil;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * presenter
 * Created by NKY on 7/31/16.
 */
class MainPresenterImpl implements MainPresenter {

    private final MainView mView;
    @Inject
    AlertLocationRepository mRepository;
    @Inject
    LocationUtil mLocationUtil;


    @Inject
    public MainPresenterImpl(MainView view) {
        mView = view;
        init();
    }

    private void init() {
        EventBusUtil.register(this);
    }


    @Override
    public void onDestroy() {
        EventBusUtil.unregister(this);
    }

    @Override
    public void handleAddSuccess(AlertLocation location) {
        App.L.d("Add Success" + location);
        loadAllLocations();
    }

    @Override
    public void onPositionPressed(Location latLng) {
        mView.showAddView(latLng);
    }


    @Override
    public void loadAllLocations() {
        mRepository
                .queryAll()
                .subscribe(new Subscriber<List<AlertLocation>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showGetAllLocationsErrorView("Error: " + e.getMessage());
                    }

                    @Override
                    public void onNext(List<AlertLocation> locations) {
                        if (ObjectUtil.isEmpty(locations)) {
                            mView.showGetAllLocationsErrorView("No Exists Locations");
                        }
                        mView.showLocationMarkers(locations);
                    }
                });
    }

    @Override
    public void onShowListPressed() {
        mView.showLocationListView();
    }

    @Subscribe
    public void onSensorChanged(Float value) {
        //float value = sensorEvent.values[0];
        mView.rotateNavigationView(value);
    }


    @Subscribe
    public void onLocationChanged(Location location) {
        mView.showCurrentLocation(location);
        mView.showAccuracy(location);
        //checkAlert(location);
    }


    @Subscribe
    public void onError(Throwable error) {
        App.L.e("PresenterImp#onError", error);
    }


    @Override
    public void onRequestLocationPressed() {

    }
}
