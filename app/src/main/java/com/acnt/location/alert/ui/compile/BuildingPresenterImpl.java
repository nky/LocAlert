package com.acnt.location.alert.ui.compile;

import com.acnt.location.alert.app.App;
import com.j256.ormlite.misc.IOUtils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * 具体的实现类
 * Created by NKY on 8/13/16.
 */
class BuildingPresenterImpl implements BuildingPresenter {

    private final BuildingView mView;

    public BuildingPresenterImpl(BuildingView view) {
        mView = view;
    }

    @Override
    public void loadBuildingInfo() {
        loadData()

                .subscribe(new Subscriber<List<String>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<String> strings) {
                        mView.showBuildInfo(strings);
                    }
                });
    }

    private Observable<List<String>> loadData() {
        return Observable
                .just(mView.getContext())
                .observeOn(Schedulers.io())
                .map(new Func1<Context, List<String>>() {
                    @Override
                    public List<String> call(Context context) {
                        App.L.d("LoadData on: " + Thread.currentThread().getName());
                        InputStream in = null;
                        List<String> data = new LinkedList<>();
                        try {
                            in = context.getResources().getAssets().open("info.txt");
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                data.add(line);
                            }
                        } catch (Exception ex) {
                            App.L.e("loadData", ex);
                            Observable.error(ex);

                        } finally {
                            IOUtils.closeQuietly(in);
                        }
                        return data;
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                ;
    }
}
