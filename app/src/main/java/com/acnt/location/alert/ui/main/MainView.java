package com.acnt.location.alert.ui.main;

import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.data.entities.Location;
import com.tencent.mapsdk.raster.model.LatLng;

import java.util.List;

/**
 * 定义UI
 * Created by NKY on 7/31/16.
 */
public interface MainView {

    /**
     * 显示添加的UI
     * @param latLng 点击的经纬度
     */
    void showAddView(Location latLng);

    /**
     * 显示所有的标记点的页面
     */
    void showLocationListView();

    void showLocationMarkers(List<AlertLocation> locations);


    void showGetAllLocationsErrorView(String msg);

    void showCurrentLocation(Location latLng);

    void showAccuracy(Location latLng);

    void rotateNavigationView(float degree);

}
