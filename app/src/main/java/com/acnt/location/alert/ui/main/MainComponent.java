package com.acnt.location.alert.ui.main;

import com.acnt.location.alert.inject.DataModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Components
 * Created by NKY on 7/31/16.
 */
@Singleton
@Component(modules = {DataModule.class, MainModule.class})
public interface MainComponent {
    void injectApp(MapsActivity activity);
}
