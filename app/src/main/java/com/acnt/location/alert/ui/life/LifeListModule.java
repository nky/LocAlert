package com.acnt.location.alert.ui.life;

import dagger.Module;
import dagger.Provides;

/**
 * 定义自己的module
 * Created by NiuKuiYuan on 2016/9/20.
 */
@Module(includes = AbsLifeListModuleProvider.class)
class LifeListModule {

    private final LifeListView mView;

    LifeListModule(LifeListView view) {
        mView = view;
    }

    @Provides
    LifeListView provideView() {
        return mView;
    }
}
