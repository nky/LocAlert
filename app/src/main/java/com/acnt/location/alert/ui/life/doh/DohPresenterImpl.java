package com.acnt.location.alert.ui.life.doh;

import com.acnt.location.alert.ui.life.LifeDataRepository;
import com.acnt.location.alert.ui.life.LifeDataRepositoryImpl;
import com.acnt.location.alert.ui.life.entities.DayOnHistory;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Presenter 具体的实现类
 * Created by NKY on 9/20/16.
 */
public class DohPresenterImpl implements DohPresenter {

    @Inject
    DohView mView;
    @Inject
    LifeDataRepository mRepository;

    @Inject
    public DohPresenterImpl() {
    }

    @Override
    public void loadData() {
        mView.showProgressView();
        mRepository.daysOnHistory()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mView.showError(throwable.getMessage());
                    }
                })
                .subscribe(new Subscriber<List<DayOnHistory>>() {
                    @Override
                    public void onCompleted() {
                      mView.hideProgressView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgressView();
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<DayOnHistory> histories) {
                        mView.showItems(histories);

                    }
                });
    }
}
