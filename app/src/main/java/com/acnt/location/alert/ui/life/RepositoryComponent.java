package com.acnt.location.alert.ui.life;

import javax.inject.Singleton;

import dagger.Component;

/**
 * 定义网络Life模块里面的网络公用的组件, 所有子component的parent组件, 方便复用.
 *
 * <img src="https://www.processon.com/chart_image/57e11301e4b0b58eb0118049.png" /><br/>
 * Created by NiuKuiYuan on 2016/9/20.
 */

@Singleton
@Component(modules = RepositoryModule.class)
public interface RepositoryComponent {

    LifeDataRepository dataRepository();
}
