package com.acnt.location.alert.ui.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;

import com.acnt.location.alert.BuildConfig;
import com.acnt.location.alert.R;
import com.acnt.location.alert.app.App;
import com.acnt.location.alert.config.Constants;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.data.entities.Location;
import com.acnt.location.alert.inject.DataModule;
import com.acnt.location.alert.ui.add.AddAlertLocationActivity;
import com.acnt.location.alert.ui.bg.LocationService;
import com.acnt.location.alert.ui.compile.BuildingActivity;
import com.acnt.location.alert.ui.life.LifeActivity;
import com.acnt.location.alert.ui.list.LocationListActivity;
import com.acnt.location.alert.ui.traffic.TrafficActivity;
import com.acnt.location.alert.util.ObjectUtil;
import com.acnt.location.alert.util.ToastUtil;
import com.jakewharton.rxbinding.view.RxView;
import com.tencent.mapsdk.raster.model.BitmapDescriptorFactory;
import com.tencent.mapsdk.raster.model.CameraPosition;
import com.tencent.mapsdk.raster.model.Circle;
import com.tencent.mapsdk.raster.model.CircleOptions;
import com.tencent.mapsdk.raster.model.IOverlay;
import com.tencent.mapsdk.raster.model.LatLng;
import com.tencent.mapsdk.raster.model.Marker;
import com.tencent.mapsdk.raster.model.MarkerOptions;
import com.tencent.tencentmap.mapsdk.map.MapView;
import com.tencent.tencentmap.mapsdk.map.TencentMap;
import com.tencent.tencentmap.mapsdk.map.UiSettings;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

public class MapsActivity extends AppCompatActivity implements MainView {

    private final static int REQ_ADD_LOCATION = 1;
    private final static int REQ_SHOW_LIST = 2;

    private static final String TAG = "MapsActivity";
    MainComponent mDataComponent;
    @BindView(R.id.toolbar)
    Toolbar mToobar;
    @BindView(R.id.map)
    MapView mTMapView;
    AlertLocation location;
    private Activity mActivity = this;

    @Inject
    MainPresenter mPresenter;

    private Marker mCurrLocation;
    private Circle mAccuracy;

    @BindView(R.id.btn_show_location)
    ImageButton mBtnLocation;

    List<AlertLocation> mLocations = new ArrayList<>();

    Map<AlertLocation, IOverlay> mOverlayMap = new HashMap<>();

    boolean mIsDragging = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        debug();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_maps);
        ButterKnife.bind(this);

        mDataComponent = DaggerMainComponent
                .builder()
                .dataModule(new DataModule(this))
                .mainModule(new MainModule(this))
                .build();
        mDataComponent.injectApp(this);
        setSupportActionBar(mToobar);
        setupMapView();
        mPresenter.onRequestLocationPressed();
        setupActions();
        startLocService();
    }

    private void debug() {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                    new StrictMode.ThreadPolicy.Builder()
                            .detectAll()
                            .penaltyLog()
                            .build()
            );

//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                    .detectActivityLeaks()
//                    .penaltyDeath()
//                    .penaltyLog()
//                    .build()
//            );
        }
    }

    private void startLocService() {
        Intent intent = new Intent(mActivity, LocationService.class);
        intent.setAction(Constants.ACTION_START_SERVICE);
        startService(intent);
    }

    private void setupActions() {
        RxView.clicks(mBtnLocation)
                .debounce(3, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        mPresenter.onRequestLocationPressed();
                    }
                });
    }


    private void setupMapView() {
        UiSettings settings = mTMapView.getUiSettings();
        settings.setAnimationEnabled(true);
        settings.setScaleControlsEnabled(true);
        settings.setZoomGesturesEnabled(true);
        settings.setScaleViewPosition(UiSettings.LOGO_POSITION_RIGHT_BOTTOM);
        TencentMap map = mTMapView.getMap();
        map.setOnMapLongClickListener(new TencentMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Location location = new Location();
                location.setLongitude(latLng.getLongitude());
                location.setLatitude(latLng.getLatitude());

                mPresenter.onPositionPressed(location);
            }
        });

        map.setOnMapCameraChangeListener(new TencentMap.OnMapCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (!mIsDragging) {
                    mTMapView.postDelayed(mDraggingMessage, TimeUnit.SECONDS.toMillis(5));
                }
                mIsDragging = true;
            }

            @Override
            public void onCameraChangeFinish(CameraPosition cameraPosition) {

            }
        });
    }

    private Runnable mDraggingMessage = new Runnable() {
        @Override
        public void run() {
            mIsDragging = false;
        }
    };


    @Override
    protected void onResume() {
        mTMapView.onResume();
        super.onResume();
        mIsDragging = false;
        App.L.d("onResume: ----------");
        mPresenter.loadAllLocations();
    }

    @Override
    protected void onPause() {
        mTMapView.onPause();
        super.onPause();
        mTMapView.removeCallbacks(mDraggingMessage);
    }

    @Override
    protected void onStop() {
        mTMapView.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mTMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.m_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.m_show_list) {
            mPresenter.onShowListPressed();
        } else if (item.getItemId() == R.id.exit) {
            Intent intent = new Intent(mActivity, LocationService.class);
            intent.setAction(Constants.ACTION_STOP_SERVICE);
            startService(intent);
            finish();
        } else if (item.getItemId() == R.id.stop_alarm) {
            Intent intent = new Intent(mActivity, LocationService.class);
            intent.setAction(Constants.ACTION_STOP_ALARM);
            startService(intent);
        } else if (item.getItemId() == R.id.build_info) {
            Intent intent = new Intent(this, BuildingActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.traffic) {
            Intent intent = new Intent(this, TrafficActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.life_info) {
            Intent intent = new Intent(this, LifeActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        App.L.d("resultCode: " + requestCode + "data: " + data);
        switch (requestCode) {
            case REQ_ADD_LOCATION:
                handleAddResult(resultCode, data);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleAddResult(int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            return;
        }

        AlertLocation loc = data.getParcelableExtra(Constants.KEY_LOCATION);
        App.L.info("save success: " + loc);
        mPresenter.handleAddSuccess(loc);
    }


    @Override
    public void showAddView(Location latLng) {
        gotoAddActivity(latLng);
    }

    private void gotoAddActivity(Location latLng) {
        Intent intent = new Intent(mActivity, AddAlertLocationActivity.class);
        AlertLocation location = new AlertLocation();
        location.setLatitude(latLng.getLatitude());
        location.setLongitude(latLng.getLongitude());
        intent.putExtra(Constants.KEY_LOCATION, location);
        mActivity.startActivityForResult(intent, REQ_ADD_LOCATION);
    }

    @Override
    public void showLocationListView() {
        gotoListActivity();
    }

    private void gotoListActivity() {
        Intent intent = new Intent(mActivity, LocationListActivity.class);
        //intent.putExtra(Constants.KEY_LOCATION_LIST, mLocations);
        startActivityForResult(intent, REQ_SHOW_LIST);
    }

    @Override
    public void showLocationMarkers(List<AlertLocation> locations) {
        TencentMap map = mTMapView.getMap();
        App.L.d("in: " + locations);

        Set<AlertLocation> uiLocations = mOverlayMap.keySet();
        App.L.d("ui: " + uiLocations);


        Collection<AlertLocation> shouldAdd = CollectionUtils.subtract(locations, uiLocations);

        if (CollectionUtils.isEqualCollection(uiLocations, locations)) {
            return;
        }

        for (AlertLocation loc : shouldAdd) {
            Marker marker = map.addMarker(
                    new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker())
                            .anchor(0.5F, 0.5F)
                            .title(loc.getAlertLabel())
                            .position(new LatLng(loc.getLatitude(), loc.getLongitude()))
            );

            mOverlayMap.put(loc, marker);
        }

        uiLocations = mOverlayMap.keySet();
        App.L.d("ui2: " + uiLocations);

        Collection<AlertLocation> shouldRemoved = CollectionUtils.subtract(uiLocations, locations);
        Iterator<AlertLocation> iterator = shouldRemoved.iterator();

        AlertLocation loc;
        while (iterator.hasNext()) {
            loc = iterator.next();
            IOverlay overlay = mOverlayMap.remove(loc);
            overlay.remove();
            //map.removeOverlay(overlay);
        }

    }

    @Override
    public void showGetAllLocationsErrorView(String msg) {
        ToastUtil.showMessage(msg);
    }

    @Override
    public void showCurrentLocation(Location latLng) {
        TencentMap map = mTMapView.getMap();
        LatLng lng = new LatLng(latLng.getLatitude(), latLng.getLongitude());
        if (mCurrLocation == null) {
            mCurrLocation = map.addMarker(new MarkerOptions()
                    .position(lng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.navigation))
                    .anchor(0.5F, 0.5F)
            );
        }

        mCurrLocation.setPosition(lng);
        if (!mIsDragging) {
            map.setCenter(lng);
            map.animateTo(lng);
            map.setZoom(16);
        }


    }

    @Override
    public void showAccuracy(Location latLng) {
        if (latLng.getAccuracy() > 100) {
            return;
        }
        TencentMap map = mTMapView.getMap();
        LatLng lng = new LatLng(latLng.getLatitude(), latLng.getLongitude());

        if (mAccuracy == null) {
            mAccuracy = map.addCircle(
                    new CircleOptions()
                            .center(lng)
                            .radius(latLng.getAccuracy())
                            .fillColor(0x440000ff)
                            .strokeWidth(0F)
            );
        }
        mAccuracy.setCenter(lng);
        mAccuracy.setRadius(latLng.getAccuracy());
    }

    @Override
    public void rotateNavigationView(float degree) {
        if (!ObjectUtil.isNull(mCurrLocation)) {
            mCurrLocation.setRotation(degree);
        }
    }
}
