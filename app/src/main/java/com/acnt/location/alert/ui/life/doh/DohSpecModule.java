package com.acnt.location.alert.ui.life.doh;

import com.acnt.location.alert.ui.life.RepositoryModule;

import dagger.Module;

/**
 * 具体的实现类
 * Created by NKY on 9/20/16.
 */
public class DohSpecModule extends RepositoryModule {
    private static final String KEY_TOH = "aec6dadb42bcb95ffa5078da43bc7316";


    public DohSpecModule(DohActivity activity) {
        mContext = activity;
    }

    @Override
    public String provideUrl() {
        return "http://api.juheapi.com";
    }

    @Override
    public String provideApiKey() {
        return KEY_TOH;
    }
}
