package com.acnt.location.alert.ui.life.doh;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.acnt.location.alert.R;
import com.acnt.location.alert.ui.life.DaggerRepositoryComponent;
import com.acnt.location.alert.ui.life.RepositoryComponent;
import com.acnt.location.alert.ui.life.entities.DayOnHistory;
import com.acnt.location.alert.util.ToastUtil;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DohActivity extends AppCompatActivity  implements DohView{

    private DohAdapter mAdapter;

    @BindView(R.id.pb_loading)
    ProgressBar mProgressBar;
    @BindView(R.id.rcv_doh)
    RecyclerView mRcvDoh;

    @Inject
    DohPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_doh);
        ButterKnife.bind(this);
        setupViews();
        setupComponents();
        mPresenter.loadData();
    }

    private void setupComponents() {
        RepositoryComponent repositoryComponent = DaggerRepositoryComponent.builder()
                .repositoryModule(new DohSpecModule(this))
                .build();
        DaggerDohComponent.builder()
                .dohModule(new DohModule(this))
                .repositoryComponent(repositoryComponent)
                .build()
                .inject(this);

    }

    private void setupViews() {
        mRcvDoh.setLayoutManager(new LinearLayoutManager(this));
        mRcvDoh.setHasFixedSize(true);

        mAdapter = new DohAdapter(this);
        mRcvDoh.setAdapter(mAdapter);

    }


    @Override
    public void showProgressView() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressView() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        ToastUtil.showMessage(message);
    }

    @Override
    public void showItems(List<DayOnHistory> histories) {
        mAdapter.updateData(histories);
    }
}
