package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.inject.DataModule;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Component;

/**
 *
 * Created by NKY on 7/30/16.
 */
@Singleton
@Component(modules = {DataModule.class, LocationListModule.class})
public interface LocationListComponent {

    void injectApp(LocationListActivity activity);

    //ListPresenter presenter();
}
