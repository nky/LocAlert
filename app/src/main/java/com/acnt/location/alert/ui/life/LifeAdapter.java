package com.acnt.location.alert.ui.life;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acnt.location.alert.R;
import com.acnt.location.alert.ui.life.entities.LifeItem;

import java.util.ArrayList;
import java.util.List;

/**
 * 定义UI的适配器
 * Created by hzniukuiyuan on 2016/9/19.
 */
public class LifeAdapter extends RecyclerView.Adapter<LifeAdapter.LifeViewHolder> {

    private List<LifeItem> mItems = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    public LifeAdapter(Context context) {
        mContext = context;
    }

    public void updateAllData(List<LifeItem> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public LifeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_life, parent, false);
        return new LifeViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(LifeViewHolder holder, int position) {
        final LifeItem item = mItems.get(position);
        holder.bindData(item);
        if (mOnItemClickListener!=null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClicked(item);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    static interface OnItemClickListener{
        void onItemClicked(LifeItem item);
    }

    static class LifeViewHolder extends RecyclerView.ViewHolder{
        TextView mTvTitle;
        TextView mTvDescription;
        public LifeViewHolder(View itemView) {
            super(itemView);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_life_title);
            mTvDescription = (TextView) itemView.findViewById(R.id.tv_life_description);
        }

        void bindData(LifeItem item) {
            mTvTitle.setText(item.getTitle());
            mTvDescription.setText(item.getDescription());
        }
    }
}
