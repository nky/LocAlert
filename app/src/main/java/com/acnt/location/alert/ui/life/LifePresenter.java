package com.acnt.location.alert.ui.life;

/**
 * 定义业务逻辑的调用.
 * Created by hzniukuiyuan on 2016/9/19.
 */
interface LifePresenter extends LifeAdapter.OnItemClickListener {

    void loadAllData();
}
