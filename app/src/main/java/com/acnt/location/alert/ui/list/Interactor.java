package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.data.entities.AlertLocation;

import java.util.List;

import rx.Observable;

/**
 * 定义业务逻辑, 也即是Model的封装
 * Created by NKY on 7/30/16.
 */
interface Interactor {

    Observable<AlertLocation> removeLocation(AlertLocation location);

    Observable<List<AlertLocation>> loadAllData();
}
