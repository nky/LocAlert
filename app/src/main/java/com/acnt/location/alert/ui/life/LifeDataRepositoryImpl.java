package com.acnt.location.alert.ui.life;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.acnt.location.alert.ui.life.entities.DayOnHistory;
import com.acnt.location.alert.ui.life.entities.LifeItem;
import com.acnt.location.alert.ui.life.entities.NetResponse;

import org.apache.commons.io.IOUtils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import dagger.Lazy;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static org.apache.commons.lang3.StringUtils.join;

/**
 * 数据的具体来源实现类
 * Created by hzniukuiyuan on 2016/9/19.
 */
public class LifeDataRepositoryImpl implements LifeDataRepository {
    @Inject
    Lazy<LifeService> mService;
    @Inject
    Context mContext;

    @Inject
    public LifeDataRepositoryImpl() {
    }

    @Override
    public Observable<List<DayOnHistory>> daysOnHistory() {
        Calendar instance = Calendar.getInstance();
        int month = instance.get(Calendar.MONTH) + 1;
        int day = instance.get(Calendar.DAY_OF_MONTH);
        return mService.get().
                daysOnHistory(String.valueOf(month), String.valueOf(day))
                .map(new Func1<NetResponse<List<DayOnHistory>>, List<DayOnHistory>>() {
                    @Override
                    public List<DayOnHistory> call(NetResponse<List<DayOnHistory>> listNetResponse) {
                        return listNetResponse.getData();
                    }
                });
    }

    @Override
    public Observable<List<LifeItem>> loadSupportLifeItems() {
        return Observable
                .just("life_items.json")
                .observeOn(Schedulers.io())
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String fileName) {
                        InputStream in = null;
                        try {

                            in = mContext.getAssets().open(fileName);
                            List<String> strings = IOUtils.readLines(in, Charset.forName("UTF-8"));
                            return join(strings, "");
                        } catch (IOException e) {
                            e.printStackTrace();
                            Observable.error(e);
                        } finally {
                            IOUtils.closeQuietly(in);
                        }
                        return null;
                    }
                })
                .map(new Func1<String, List<LifeItem>>() {
                    @Override
                    public List<LifeItem> call(String s) {
                        Type t = new TypeToken<List<LifeItem>>() {
                        }.getType();
                        Gson gson = new Gson();
                        return gson.fromJson(s, t);
                    }
                })

                ;
    }


}
