package com.acnt.location.alert.ui.list;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewStub;

import com.acnt.location.alert.R;
import com.acnt.location.alert.app.App;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.inject.DataModule;
import com.acnt.location.alert.util.ToastUtil;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationListActivity extends AppCompatActivity implements
        AlertLocationAdapter.OnItemViewClickListener,
        LocationsListView {
    LocationListComponent mListComponent;

    @Inject
    ListPresenter mPresenter;
    @BindView(R.id.vs_empty)
    ViewStub mVsEmpty;
    @BindView(R.id.rcv_locations)
    RecyclerView mRvLocations;

    private AlertLocationAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_location_list);
        setupComponents();
        ButterKnife.bind(this);
        setupViews();
        mPresenter.loadData();
    }

    private void setupViews() {
        mAdapter = new AlertLocationAdapter(this);
        mAdapter.setItemClickListener(this);
        mRvLocations.setLayoutManager(new LinearLayoutManager(this));
        mRvLocations.setAdapter(mAdapter);
    }

    private void setupComponents() {
        mListComponent = DaggerLocationListComponent
                .builder()
                .dataModule(new DataModule(this))
                .locationListModule(new LocationListModule(this))
                .build();
        mListComponent.injectApp(this);
        //mPresenter = mListComponent.presenter();
    }

    @Override
    public void showEmptyView() {
        mRvLocations.setVisibility(View.GONE);
        mVsEmpty.setVisibility(View.VISIBLE);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

    }

    @Override
    public void showLocationListView(List<AlertLocation> locations) {
        App.L.d(locations);
        mVsEmpty.setVisibility(View.GONE);
        mRvLocations.setVisibility(View.VISIBLE);
        mAdapter.updateData(locations);

    }

    @Override
    public void showChoiceView(final AlertLocation location) {
        new AlertDialog.Builder(this)
                .setTitle("删除提醒")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("删除之后就不会得到提醒了?")
                .setNegativeButton("不删除", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setPositiveButton("确定删除", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        mPresenter.deleteLocation(location);
                    }
                })
                .create()
                .show();

    }

    @Override
    public AlertLocationAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void onItemClicked(AlertLocation location, int position) {
        mPresenter.onItemClicked(location);
    }

    @Override
    public void showDeleteErrorView(String msg) {
        ToastUtil.showMessage(msg);
    }

    //@OnClick(R.id.back)
    public void back() {
        mPresenter.onBackPressed();
    }

    @Override
    public void backView() {
        onBackPressed();
    }
}
