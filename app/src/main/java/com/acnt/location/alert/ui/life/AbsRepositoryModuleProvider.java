package com.acnt.location.alert.ui.life;

import dagger.Binds;
import dagger.Module;

/**
 * 定义抽象类的绑定
 * Created by NiuKuiYuan on 2016/9/20.
 */
@Module
public abstract class AbsRepositoryModuleProvider {

    @Binds
    public abstract LifeDataRepository provideLifeDataRepository(LifeDataRepositoryImpl repository);
}
