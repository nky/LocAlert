package com.acnt.location.alert.ui.life;

import com.acnt.location.alert.ui.life.entities.DayOnHistory;
import com.acnt.location.alert.ui.life.entities.LifeItem;

import java.util.List;

import rx.Observable;

/**
 * 定义生活服务的数据仓库
 * Created by hzniukuiyuan on 2016/9/19.
 */
public interface LifeDataRepository {

    Observable<List<DayOnHistory>> daysOnHistory();

    Observable<List<LifeItem>> loadSupportLifeItems();
}
