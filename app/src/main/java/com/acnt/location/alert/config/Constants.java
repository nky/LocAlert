package com.acnt.location.alert.config;

/**
 * 应用的一些常量
 * Created by NKY on 7/29/16.
 */
public interface Constants {

    String KEY_LOCATION = "key_location";
    String KEY_LOCATION_LIST = "key_location_list";

    String ACTION_START_SERVICE = "com.acnt.location.alert.ACTION_START_SERVICE";
    String ACTION_STOP_SERVICE = "com.acnt.location.alert.ACTION_STOP_SERVICE";
    String ACTION_STOP_ALARM = "com.acnt.location.alert.ACTION_STOP_ALRAM";
}
