package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.app.App;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.acnt.location.alert.util.ToastUtil;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * Presenter
 * Created by NKY on 7/30/16.
 */
public class ListPresenterImpl implements ListPresenter {

    LocationsListView mView;
    Interactor mInteractor;

    @Inject
    public ListPresenterImpl(LocationsListView view, Interactor interactor) {
        mInteractor = interactor;
        mView = view;
    }


    @Override
    public void onItemClicked(AlertLocation location) {
        mView.showChoiceView(location);
    }


    @Override
    public void deleteLocation(AlertLocation location) {
        mInteractor
                .removeLocation(location)
                .subscribe(new Subscriber<AlertLocation>() {
                    @Override
                    public void onCompleted() {
                        App.L.d("onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showDeleteErrorView("RU kidding me?!\n" + e.getMessage());
                    }

                    @Override
                    public void onNext(AlertLocation location) {
                        App.L.d("onNext: " + location);
                        loadData();

                    }
                })
        ;
    }

    @Override
    public void loadData() {
        mInteractor
                .loadAllData()
                .subscribe(new Subscriber<List<AlertLocation>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showEmptyView();
                        ToastUtil.showMessage("Error: " + e);

                    }

                    @Override
                    public void onNext(List<AlertLocation> locations) {
                        if (CollectionUtils.isEmpty(locations)) {
                            mView.showEmptyView();
                        } else {
                            mView.showLocationListView(locations);
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        mView.backView();
    }
}
