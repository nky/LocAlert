package com.acnt.location.alert.ui.life;

import com.acnt.location.alert.ui.life.entities.DayOnHistory;
import com.acnt.location.alert.ui.life.entities.NetResponse;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 生活相关的服务
 * Created by hzniukuiyuan on 2016/9/19.
 */
interface LifeService {

    @GET("/japi/toh")
    Observable<NetResponse<List<DayOnHistory>>> daysOnHistory(@Query("month") String month, @Query("day")String day);
}
