package com.acnt.location.alert.ui.main;

import dagger.Binds;
import dagger.Module;

/**
 *
 * Created by NKY on 7/31/16.
 */
@Module
abstract class LogicModule {
    @Binds
    public abstract MainPresenter providePresenter(MainPresenterImpl presenter);
}
