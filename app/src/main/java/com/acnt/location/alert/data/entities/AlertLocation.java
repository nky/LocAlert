package com.acnt.location.alert.data.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.tencent.tencentmap.mapsdk.map.Projection;
import com.tencent.tencentmap.mapsdk.map.TencentMap;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 要报警的点
 * Created by NKY on 7/28/16.
 */
@Data
@Accessors(prefix = "m")
@ToString
@DatabaseTable(tableName = "alert_location")
public class AlertLocation implements Parcelable {

    public AlertLocation() {
    }

    /**
     * 存储的ID
     */
    @DatabaseField(generatedId = true)
    private UUID mLocationId;
    /**
     * 存储的位置
     */
    @DatabaseField
    private double mLatitude;
    @DatabaseField
    private double mLongitude;

    /**
     * 警告的半径
     */
    @DatabaseField
    private long mAlertRadius;

    /**
     * 该警告点的名称, 先在使用手写的方式
     */
    @DatabaseField
    private String mAlertLabel;

    @DatabaseField
    private long mCreateDate;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.mLocationId);
        dest.writeDouble(this.mLatitude);
        dest.writeDouble(this.mLongitude);
        dest.writeLong(this.mAlertRadius);
        dest.writeString(this.mAlertLabel);
        dest.writeLong(this.mCreateDate);
    }

    protected AlertLocation(Parcel in) {
        this.mLocationId = (UUID) in.readSerializable();
        this.mLatitude = in.readDouble();
        this.mLongitude = in.readDouble();
        this.mAlertRadius = in.readLong();
        this.mAlertLabel = in.readString();
        this.mCreateDate = in.readLong();
    }


    private final static double EARTH_RADIUS = 6378137;

    private static double rad(double d)
    {
        return Math.toRadians(d);
    }
    /**
     * 判断该位置的覆盖范围是否包含对于的地点
     */
    public boolean contains(Location location) {
        return getDistance(location.getLatitude(), location.getLongitude(), mLatitude, mLongitude) < mAlertRadius;
    }

    private static double getDistance(double lat1, double lng1, double lat2, double lng2)
    {
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) +
                Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
        s = s * EARTH_RADIUS;
        s = Math.round(s * 10000) / 10000;
        return s;
    }

    public static final Parcelable.Creator<AlertLocation> CREATOR = new Parcelable.Creator<AlertLocation>() {
        @Override
        public AlertLocation createFromParcel(Parcel source) {
            return new AlertLocation(source);
        }

        @Override
        public AlertLocation[] newArray(int size) {
            return new AlertLocation[size];
        }
    };
}
