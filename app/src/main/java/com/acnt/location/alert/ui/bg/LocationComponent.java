package com.acnt.location.alert.ui.bg;

import com.acnt.location.alert.inject.DataModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Component
 * Created by NKY on 8/8/16.
 */
@Singleton
@Component(modules = {LocationModule.class, DataModule.class})
public interface LocationComponent {

    void injectApp(LocationService service);

}
