package com.acnt.location.alert.util;

import com.acnt.location.alert.data.entities.Location;
import com.tencent.map.geolocation.TencentLocation;
import com.tencent.map.geolocation.TencentLocationListener;
import com.tencent.map.geolocation.TencentLocationManager;
import com.tencent.map.geolocation.TencentLocationRequest;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.Subscriber;

/**
 * 定位功能的工具类
 * Created by NKY on 8/7/16.
 */
@Singleton
public class LocationUtil implements SensorEventListener {


    private final Context mContext;

    private TencentLocationManager mLocationManager;
    private TencentLocationRequest mLocationRequest;

    private Sensor mSensor;
    private SensorManager mSensorManager;

    private final long INTERVAL_LONG = 10 * 1000;
    private final long INTERVAL_SHORT = 2 * 1000;
    private TencentLocationListener mTencentLocationListener;

    @Inject
    public LocationUtil(Context context) {
        mContext = context;
        init();
        bindListener();
    }

    private void init() {
        mSensorManager = (SensorManager) mContext.getSystemService(Activity.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
    }

    private void bindListener() {
        mLocationManager = TencentLocationManager.getInstance(mContext);
        mLocationRequest = TencentLocationRequest
                .create()
                .setRequestLevel(TencentLocationRequest.REQUEST_LEVEL_GEO)
                .setInterval(INTERVAL_LONG)
                .setAllowCache(true)
        ;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float value = sensorEvent.values[0];
        EventBusUtil.post(value);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }


    public Observable<Location> requestCurrLocation() {
        Observable.OnSubscribe<Location> subscribe = new Observable.OnSubscribe<Location>() {
            @Override
            public void call(final Subscriber<? super Location> subscriber) {
                //由于是连续的回调, 因此不应该调用onCompleted()
                mTencentLocationListener = new TencentLocationListener() {

                    @Override
                    public void onLocationChanged(TencentLocation tencentLocation, int code, String msg) {

                        if (subscriber.isUnsubscribed()) {
                            return;
                        }

                        if (code != TencentLocation.ERROR_OK) {
                            subscriber.onError(new IllegalArgumentException("code: " + code + " msg: " + msg));
                        } else {
                            Location location = new Location();
                            location.setLatitude(tencentLocation.getLatitude());
                            location.setLongitude(tencentLocation.getLongitude());
                            location.setAccuracy(tencentLocation.getAccuracy());

                            subscriber.onNext(location);

                            //由于是连续的回调, 因此不应该调用onCompleted()
                            //subscriber.onCompleted();
                        }
                    }

                    @Override
                    public void onStatusUpdate(String s, int i, String s1) {

                    }
                };
                int error = mLocationManager.requestLocationUpdates(mLocationRequest, mTencentLocationListener);

                String msg = "";
                switch (error) {
                    case 0:
                        msg = "成功注册监听器";
                        break;
                    case 1:
                        msg = "设备缺少使用腾讯定位服务需要的基本条件";
                        break;
                    case 2:
                        msg = "manifest 中配置的 key 不正确";
                        break;
                    case 3:
                        msg = "自动加载libtencentloc.so失败";
                        break;
                    default:
                        break;
                }
                if (error != 0) {
                    subscriber.onError(new IllegalArgumentException(msg));
                }
                mSensorManager.registerListener(LocationUtil.this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        };
        return Observable.create(subscribe);
    }


    public void recycle() {
        mSensorManager.unregisterListener(this);
        if (mTencentLocationListener != null) {
            mLocationManager.removeUpdates(mTencentLocationListener);
        }
    }
}
