package com.acnt.location.alert.ui.life.entities;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 网络请求类的封装
 * Created by NKY on 9/21/16.
 */
@Data
@Accessors(prefix = "m")
public class NetResponse<T> {
    @SerializedName("error_code")
    private int mErrorCode;
    @SerializedName("resonse")
    private String mMessage;
    @SerializedName("result")
    private T mData;
}
