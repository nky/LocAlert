package com.acnt.location.alert.ui.compile;

/**
 * 编译信息的Presenter
 * Created by NKY on 8/13/16.
 */
interface BuildingPresenter {

    void loadBuildingInfo();
}
