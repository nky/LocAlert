package com.acnt.location.alert.util;

import android.text.TextUtils;

import java.util.Collection;
import java.util.Collections;

/**
 * 常用的一些工具类
 * Created by NKY on 7/30/16.
 */
public class ObjectUtil {

    public static boolean isEmpty(String str) {
        return isNull(str)|| TextUtils.isEmpty(str);
    }

    public static boolean isNull(Object o) {
        return o == null;
    }

    public static boolean isEmpty(Collection c) {
        return isNull(c) || c.isEmpty();
    }
}
