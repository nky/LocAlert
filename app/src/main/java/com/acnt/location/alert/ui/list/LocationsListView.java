package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.data.entities.AlertLocation;

import java.util.List;

/**
 * 定义列表页面的UI视图
 * Created by NKY on 7/30/16.
 */
public interface LocationsListView {

    /**
     * 展示空列表视图
     */
    void showEmptyView();

    /**
     * 显示地点的列表
     */
    void showLocationListView(List<AlertLocation> locations);


    /**
     * 显示要处理的数据
     *
     * @param location 要处理的数据
     */
    void showChoiceView(AlertLocation location);

    /**
     * 获取View的Adapter
     *
     * @return Adapter
     */
    AlertLocationAdapter getAdapter();

    /**
     * 显示删除出错的View
     * @param msg 要显示的错误信息
     */
    void showDeleteErrorView(String msg);

    /**
     * 回到上个页面
     */
    void backView();


}
