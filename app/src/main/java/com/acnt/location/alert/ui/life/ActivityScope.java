package com.acnt.location.alert.ui.life;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * 定义自己的Scope
 * Created by NiuKuiYuan on 2016/9/20.
 */
@Scope
@Documented
@Retention(value= RetentionPolicy.RUNTIME)
public @interface ActivityScope {
}
