package com.acnt.location.alert.data;

import com.acnt.location.alert.data.entities.AlertLocation;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * 具体操作DB的类
 * Created by NKY on 7/28/16.
 */
@Singleton
public class DbHelper extends OrmLiteSqliteOpenHelper {
    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "locations.db";

    @Inject
    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, AlertLocation.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            TableUtils.dropTable(connectionSource, AlertLocation.class,true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        onCreate(database, connectionSource);

    }
}
