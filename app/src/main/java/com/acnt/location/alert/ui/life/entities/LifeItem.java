package com.acnt.location.alert.ui.life.entities;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 定义生活服务的数据模型
 * Created by hzniukuiyuan on 2016/9/19.
 */
@Data
@Accessors(prefix = "m")
public class LifeItem {
    @SerializedName("title")
    private String mTitle;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("type")
    private int mType;
}
