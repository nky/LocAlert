package com.acnt.location.alert.ui.traffic;

import com.acnt.location.alert.R;

import android.content.pm.PackageManager;
import android.net.TrafficStats;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrafficActivity extends AppCompatActivity {

    @BindView(R.id.tv_traffic_all)
    TextView mTvTrafficAll;
    @BindView(R.id.tv_battery)
    TextView mTvBattery;
    @BindView(R.id.tv_send)
    TextView mTvTrafficSend;
    @BindView(R.id.tv_recv)
    TextView mTvTrafficRecv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_traffic);
        ButterKnife.bind(this);
        computeStatus();
    }

    private void computeStatus() {
        try {
            int uid =  getPackageManager().getApplicationInfo(getPackageName(), 0).uid;
            long recvBytes = TrafficStats.getUidRxBytes(uid);
            long sendBytes = TrafficStats.getUidTxBytes(uid);
            //TrafficStats.getUid
            setValue(mTvTrafficRecv, recvBytes);
            setValue(mTvTrafficSend, sendBytes);
            setValue(mTvTrafficAll, recvBytes + sendBytes);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setValue(TextView txt, long value) {
        txt.setText(String.format("%,d", value));
    }
}
