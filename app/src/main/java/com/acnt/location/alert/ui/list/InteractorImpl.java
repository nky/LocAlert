package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.data.AlertLocationRepository;
import com.acnt.location.alert.data.entities.AlertLocation;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * 具体的业务逻辑的实现
 * Created by NKY on 7/30/16.
 */
public class InteractorImpl implements Interactor {

    final AlertLocationRepository mRepository;

    @Inject
    public InteractorImpl(AlertLocationRepository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<AlertLocation> removeLocation(AlertLocation location) {
        return mRepository.remove(location);
    }

    @Override
    public Observable<List<AlertLocation>> loadAllData() {
        return mRepository.queryAll();
    }
}
