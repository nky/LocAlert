package com.acnt.location.alert.ui.list;

import com.acnt.location.alert.data.AlertLocationRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * 定义List的Module
 * Created by NKY on 7/30/16.
 */
@Module(includes = LogicModule.class)
class LocationListModule {

    private final LocationsListView mView;

    public LocationListModule(LocationsListView view) {
        mView = view;
    }

    @Provides
    public LocationsListView provideListView() {
        return mView;
    }



//    @Singleton
//    @Provides
//    public Interactor provideInteractor(AlertLocationRepository repository) {
//        return new InteractorImpl(repository);
//    }
}
