package com.acnt.location.alert.ui.bg;

import com.acnt.location.alert.util.LocationUtil;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * LocationService
 * Created by NKY on 8/8/16.
 */
@Module
public class LocationModule {

    private final Context mContext;

    public LocationModule(Context context) {
        mContext = context;
    }


    @Provides
    public LocationUtil provideLocationUtil(Context context) {
        return new LocationUtil(context);
    }
}
