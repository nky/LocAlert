package com.acnt.location.alert.ui.life.doh;

import com.acnt.location.alert.ui.life.entities.DayOnHistory;

import java.util.List;

/**
 * 历史上的今天的view抽象
 * Created by NKY on 9/20/16.
 */

interface DohView {

    void showProgressView();

    void hideProgressView();

    void showError(String message);

    void showItems(List<DayOnHistory> histories);
}
