package com.acnt.location.alert.ui.life.doh;

import dagger.Binds;
import dagger.Module;

/**
 * 抽象类的绑定
 * Created by NKY on 9/20/16.
 */
@Module
abstract class AbsDohModuleProvider {

    @Binds
    public abstract DohPresenter prividePresenter(DohPresenterImpl presenter);
}
