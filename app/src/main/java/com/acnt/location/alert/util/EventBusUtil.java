package com.acnt.location.alert.util;

import org.greenrobot.eventbus.EventBus;

/**
 * EventBus Util
 * Created by NKY on 8/7/16.
 */
public class EventBusUtil {

    public static void register(Object obj) {
        EventBus bus = getBus();
        if (bus.isRegistered(obj)) {
            return;
        }
        bus.register(obj);

    }

    public static void unregister(Object object) {
        getBus().unregister(object);
    }

    public static void post(Object msg) {
        getBus().post(msg);

    }

    private static EventBus getBus() {
        return EventBus.getDefault();
    }
}
