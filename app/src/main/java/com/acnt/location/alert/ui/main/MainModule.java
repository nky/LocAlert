package com.acnt.location.alert.ui.main;

import dagger.Module;
import dagger.Provides;

/**
 * Module
 * Created by NKY on 7/31/16.
 */
@Module(includes = LogicModule.class)
class MainModule {

    private final MainView mView;

    public MainModule(MainView view) {
        mView = view;
    }

    @Provides
    public MainView provideMainView() {
        return mView;
    }
}
