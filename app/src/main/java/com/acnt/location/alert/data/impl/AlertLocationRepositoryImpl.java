package com.acnt.location.alert.data.impl;

import com.acnt.location.alert.data.AlertLocationRepository;
import com.acnt.location.alert.data.DbHelper;
import com.acnt.location.alert.data.entities.AlertLocation;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * 具体的实现类
 * Created by NKY on 7/28/16.
 */
public class AlertLocationRepositoryImpl implements AlertLocationRepository {

    private final DbHelper mDbHelper;

    public AlertLocationRepositoryImpl(DbHelper dbHelper) {
        mDbHelper = dbHelper;
    }

    private Dao<AlertLocation, UUID> getDao() throws SQLException {
        return mDbHelper.getDao(AlertLocation.class);
    }

    @Override
    public Observable<AlertLocation> save(final AlertLocation location) {
        return Observable
                .fromCallable(new Callable<AlertLocation>() {
                    @Override
                    public AlertLocation call() throws Exception {
                        //location.setLocationId(UUID.randomUUID().toString());
                        Dao<AlertLocation, UUID> dao = getDao();
                        dao.create(location);
                        return location;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    private AlertLocation removeSycn(AlertLocation location) throws Exception {
        if (location.getLocationId() == null) {
            IllegalArgumentException exception =
                    new IllegalArgumentException("Data should retrieve from cache &" +
                            "the ID Would not be NULL");
            throw exception;
        }
        getDao().deleteById(location.getLocationId());
        return location;
    }

    @Override
    public Observable<AlertLocation> remove(final AlertLocation location) {
        return Observable
                .fromCallable(new Callable<AlertLocation>() {
                    @Override
                    public AlertLocation call() throws Exception {
                        AlertLocation loc = removeSycn(location);
                        return loc;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                ;
    }

    @Override
    public Observable<List<AlertLocation>> queryAll() {
        return Observable
                .fromCallable(new Callable<List<AlertLocation>>() {
                    @Override
                    public List<AlertLocation> call() throws Exception {
                        return getDao().queryForAll();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
