位置提醒
====

[MENU]

项目的目的
-----
- 对要使用到的开源组件进行组合整理, 搭建一个简单的框架,方便后续的开发, 其中包括
 - MVP
> 参考 `ui.main` 或者 `ui.list`

- Dagger2
> 见 `main.MainModule, main.MainComponent`
 - EventBus3
 > 见 `EventBusUtil`

- OrmLite
> 见`data`

- advancedandroidlogger
> @see `App.java`

- lombok
> @See `data.entities`

- RxJava
> `ui`

- RxAndroid
- TestCase
 
 
 
项目的进度
-----
 - [x] 组件的引入和代码的编写
 - [x] ProGuard的编写
 - [x] 版本的控制
 - [ ] TestCase的编写
 - [x] 流量统计的实现



应用的使用
----
- 主页, 长安地图出现添加页面   
![main](./captures/main.png)

- 添加页面   
![add](./captures/add.png)

- 列表页面 长安item出现删除提示   
![list](./captures/list.png)
 
- 删除页面   
![main](./captures/delete.png)
- 项目的提交历史   
![main](./captures/compile.png)

- 应用的统计   
![main](./captures/traffic.png)
 
 
